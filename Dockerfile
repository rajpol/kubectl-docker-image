FROM alpine:latest

RUN apk update && apk add --no-cache curl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.24.0/bin/linux/amd64/kubectl
RUN chmod +x kubectl
RUN mv kubectl /usr/local/bin/

CMD ["kubectl", "version"]
